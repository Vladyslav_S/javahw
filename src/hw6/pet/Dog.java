package hw6.pet;

import hw6.enums.Species;

public class Dog extends Pet implements Foulable {

    public Dog(String nickname) {
        super(nickname);
        super.species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.DOG;
    }

    public Dog() {
        super.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.nickname + ". Я соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Tapki destroyed");
    }
}
