package hw6.pet;

import hw6.enums.Species;

public class DomesticCat extends Pet implements Foulable {
    public DomesticCat(String nickname) {
        super(nickname);
        super.species = Species.DOMESTIC_CAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.DOMESTIC_CAT;
    }

    public DomesticCat() {
        super.species = Species.DOMESTIC_CAT;
    }

    @Override
    public void respond() {

    }

    @Override
    public void foul() {
        System.out.println("Kolbasa eliminated");
    }
}
