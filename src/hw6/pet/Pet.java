package hw6.pet;

import hw6.enums.Species;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    protected Species species = Species.UNKNOWN;
    protected String nickname;
    protected int age;
    protected int trickLevel;
    protected String[] habits;

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {

    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public abstract void respond();

    @Override
    public boolean equals(Object pet) {
        if (pet instanceof Pet) {
            Pet newPet = (Pet)pet;
            boolean nullNames = this.nickname == null && newPet.nickname == null;

            return (nullNames || (this.nickname != null && this.nickname.equals(newPet.nickname))) && this.age == newPet.age;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age);
    }

    @Override
    public String toString() {
        return species + "{nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=[" + Arrays.toString(this.habits) + "]}";
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
