package hw6.pet;

import hw6.enums.Species;

public class RoboCat extends Pet implements Foulable{

    public RoboCat(String nickname) {
        super(nickname);
        super.species = Species.ROBO_CAT;
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.ROBO_CAT;
    }

    public RoboCat() {
        super.species = Species.ROBO_CAT;
    }

    @Override
    public void respond() {

    }

    @Override
    public void foul() {
        System.out.println("LAGALO!");
    }
}
