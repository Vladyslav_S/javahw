package hw6.pet;

import hw6.enums.Species;

public class Fish extends Pet{
    public Fish(String nickname) {
        super(nickname);
        super.species = Species.FISH;
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.FISH;
    }

    public Fish() {
        super.species = Species.FISH;
    }

    @Override
    public void respond() {

    }
}
