package hw6.humans;

public final class Woman extends Human {
    @Override
    public void greetPet() {
        System.out.println("Привет, " + super.family.getPet().getNickname() + "Я баба!");
    }

    public void makeup() {
        System.out.println("Поджираю по тихому");
    }
}
