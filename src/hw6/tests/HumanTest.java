package hw6.tests;

import hw6.humans.Human;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HumanTest {

    @Test
    public void testToString() {
        Human human = new Human();
        String result = human.toString().split("[{]")[0];

        assertEquals(result, "Human");
    }
}