package hw6.tests;

import hw6.Family;
import hw6.humans.Human;
import hw6.pet.Fish;
import hw6.pet.Pet;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class FamilyTest {

    @Test
    public void testToString() {
        Family family = new Family(new Human(), new Human());

        String result = family.toString().split("[{]")[0];

        assertEquals(result, "Family");
    }

    @Test
    public void deleteChildHumanArgument_shouldDeleteChosenChild() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(kid2);

        assertArrayEquals(family.getChildren(), new Human[]{kid1, kid3});
    }

    @Test
    public void deleteChild_shouldNotDeleteChildIfItNotInTheFamily() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        Human randomKid = new Human();
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(randomKid);

        assertArrayEquals(family.getChildren(), new Human[]{kid1, kid2, kid3});
    }

    @Test
    public void deleteChildIndexArgument_shouldDeleteChosenChild() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(2);

        assertArrayEquals(family.getChildren(), new Human[]{kid1, kid2});
    }


    @Test
    public void deleteChildByIndex_shouldNotDeleteChildIfItNotInTheFamily() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        Human randomKid = new Human();
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(4);

        assertArrayEquals(family.getChildren(), new Human[]{kid1, kid2, kid3});
    }

    @Test
    public void addChild() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        family.setChildren(new Human[]{kid1});
        Human[] childrenStartArr = family.getChildren();

        family.addChild(kid2);

        assertEquals(family.getChildren().length, childrenStartArr.length + 1);
        assertEquals(family.getChildren()[1], kid2);
    }

    @Test
    public void countFamily() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Pet pet = new Fish();

        family.addChild(kid1);
        family.setPet(pet);

        assertEquals(family.countFamily(), 3);
    }
}