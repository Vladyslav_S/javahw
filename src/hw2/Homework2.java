package hw2;

import java.util.Random;
import java.util.Scanner;

public class Homework2 {
    private static final int SIZE = 5;
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        char[][] field = initField(SIZE);

        System.out.println("All set. Get ready to rumble!");

        printField(field);
        int[] target = initTarget(SIZE);

        while (true) {
            System.out.println("Enter aim row number:");

            String row;
            row = scanner.nextLine();

            while (!isValidAim(row)) {
                System.out.printf("Row should be from 1 to %s inclusive! Please, enter again\n", SIZE);
                row = scanner.nextLine();
            }

            System.out.println("Enter aim column number:");

            String column;
            column = scanner.nextLine();
            while (!isValidAim(column)) {
                System.out.printf("Column should be from 1 to %s inclusive! Please, enter again\n", SIZE);
                column = scanner.nextLine();
            }

            int x = Integer.parseInt(row) - 1;
            int y = Integer.parseInt(column) - 1;

            if (isHit(x, y, target)) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        field[target[0]+i][target[1]+j] = 'x';
                    }
                }
                System.out.println("You have won!");
                printField(field);
                break;
            } else {
                field[x][y] = '*';
                printField(field);
            }
        }
    }

    private static char[][] initField(int size) {
        char[][] result = new char[size][];
        for (int i = 0; i < size; i++) {
            result[i] = new char[size];
            for (int j = 0; j < size; j++) {
                result[i][j] = '-';
            }
        }
        return result;
    }

    private static void printField(char[][] field) {
        for (int i = 0; i <= field.length; i++) {
            System.out.print(i + "|");
        }
        System.out.println();
        for (int i = 0; i < field.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < field[i].length; j++) {
                System.out.print("|" + field[i][j]);
            }
            System.out.println("|");
        }
    }

    private static int[] initTarget(int size) {
        Random randomizer = new Random();
        int x = randomizer.nextInt(size - 2);
        int y = randomizer.nextInt(size - 2);
        return new int[]{x, y};
    }

    private static boolean isValidAim(String input) {
        try {
            int x = Integer.parseInt(input);
            return 0 < x && x <= SIZE;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isHit(int x, int y, int[] target) {
        return target[0] <= x && x <= target[0] + 2 && target[1] <= y && y <= target[1] + 2;
    }

}
