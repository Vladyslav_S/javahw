package hw7.tests;

import hw7.pet.Pet;
import hw7.pet.RoboCat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PetTest {

    @Test
    public void testToString() {
        Pet pet = new RoboCat("murzik");
        String result = pet.toString().split("[{]")[0];

        assertEquals(result, pet.getSpecies().toString());
    }
}