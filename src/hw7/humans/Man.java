package hw7.humans;

public final class Man extends Human {
    @Override
    public void greetPet() {
        System.out.println("Привет, " + super.family.getPet().getNickname() + "Я мужик!");
    }

    public void repairCar() {
        System.out.println("Бухаю по тихому");
    }
}
