package hw8.tests;

import hw8.pet.Pet;
import hw8.pet.RoboCat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PetTest {

    @Test
    public void testToString() {
        Pet pet = new RoboCat("murzik");
        String result = pet.toString().split("[{]")[0];

        assertEquals(result, pet.getSpecies().toString());
    }
}