package hw8.pet;

import hw8.enums.Species;

import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickname) {
        super(nickname);
        super.species = Species.FISH;
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.FISH;
    }

    public Fish() {
        super.species = Species.FISH;
    }

    @Override
    public void respond() {

    }
}
