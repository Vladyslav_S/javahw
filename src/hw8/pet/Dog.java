package hw8.pet;

import hw8.enums.Species;

import java.util.Set;

public class Dog extends Pet implements Foulable {

    public Dog(String nickname) {
        super(nickname);
        super.species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.DOG;
    }

    public Dog() {
        super.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + super.nickname + ". Я соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Tapki destroyed");
    }
}
