package hw8;

import hw8.humans.Human;
import hw8.interfaces.FamilyDao;
import hw8.pet.Pet;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        for (int i = 0; i < families.size(); i++) {
            System.out.println(i + " " + families.get(i));
        }
    }

    public List<Family> getFamiliesBiggerThan(int familySize) {
        List<Family> filteredFamily = familyDao.getAllFamilies().stream().filter(family -> family.countFamily() > familySize).toList();
        System.out.println(filteredFamily);
        return filteredFamily;
    }

    public List<Family> countFamiliesWithMemberNumber(int familySize) {
        List<Family> filteredFamily = familyDao.getAllFamilies().stream().filter(family -> family.countFamily() < familySize).toList();
        System.out.println(filteredFamily);
        return filteredFamily;
    }

    public void createNewFamily(Human father, Human mother) {
        familyDao.saveFamily(new Family(father, mother));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        String kidName = new Random().nextInt(2) > 0 ? boyName : girlName;
        Human kid = new Human(kidName, family.getFather().getSurname(), Calendar.getInstance().get(Calendar.YEAR));
        family.addChild(kid);
        return family;
    }

    public Family adoptChild(Family family, Human kid) {
        family.addChild(kid);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        for (Family family : familyDao.getAllFamilies()) {
            for (int i = 0; i < family.getChildren().size(); i++) {
                 Human child = family.getChildren().get(i);
                if (Calendar.getInstance().get(Calendar.YEAR) - child.getYear() > age){
                    family.deleteChild(i);
                }
            }
        }
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    List<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPets();
    }

    void addPet(int familyIndex, Pet pet){
        familyDao.getFamilyByIndex(familyIndex).setPets(pet);
    }
}
