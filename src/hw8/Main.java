package hw8;

import hw8.enums.DayOfWeek;
import hw8.humans.Human;
import hw8.humans.Man;
import hw8.humans.Woman;
import hw8.interfaces.FamilyDao;
import hw8.pet.DomesticCat;

import java.util.EnumMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Man fatherVasia = new Man();
        Woman motherVasia = new Woman();
        DomesticCat murzik = new DomesticCat();
        Map<DayOfWeek, String> schedule = new EnumMap<>(DayOfWeek.class);

        schedule.put(DayOfWeek.MONDAY, "eat");

        Human klara = new Human("Klara", "Pupkin", 1900);
        Human vasia = new Human("Vasia", "Pupkin", 1900, motherVasia, fatherVasia);

        Family family1 = new Family(klara, vasia);

        Human kid1 = new Human("Vasia", "Pupkin", 1974, 80,  klara, vasia, schedule);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);

        family1.setPets(murzik);
        family1.setChildren(new Human[]{kid1, kid2});
        family1.addChild(kid3);
        family1.deleteChild(1);

        murzik.eat();
        murzik.respond();
        murzik.foul();
        System.out.println(murzik);

        kid1.describePet();
        kid1.greetPet();
        System.out.println(kid1);

        for (int i = 0; i < 100000; i++) {
            Family family = new Family(new Human(), new Human());
        }

        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.createNewFamily(vasia, klara);
        familyController.getAllFamilies();
        familyController.displayAllFamilies();
        familyController.getFamiliesBiggerThan(1);
        familyController.countFamiliesWithMemberNumber(2);
        familyController.bornChild(familyController.getAllFamilies().get(0), "serega", "masha" );
        familyController.adoptChild(familyController.getAllFamilies().get(0), kid1);
        familyController.deleteAllChildrenOlderThen(5);
        familyController.count();
        familyController.getFamilyById(0);
        familyController.addPet(0, murzik);
        familyController.getPets(0);
        familyController.deleteFamilyByIndex(0);
    }
}
