package hw8;

import hw8.humans.Human;
import hw8.pet.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private List<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.pets = new ArrayList<>();

        mother.setFamily(this);
        father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        for (Human child : children) {
            child.setFamily(this);
            this.children.add(child);
        }
    }

    public void setPets(Pet pets) {
        this.pets.add(pets);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + (children) +
                ", pet=" + pets +
                '}';
    }

    public void addChild(Human kid) {
        this.children.add(kid);
        kid.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (this.children.size() <= index || index < 0) {
            return false;
        }
        this.children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        return this.children.remove(child);
    }

    public int countFamily() {
        int counter = 0;
        if (mother != null) {
            counter++;
        }
        if (father != null) {
            counter++;
        }
        return counter + children.size();
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }
}
