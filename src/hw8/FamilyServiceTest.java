package hw8;

import hw8.humans.Human;
import hw8.interfaces.FamilyDao;
import hw8.pet.Pet;
import hw8.pet.RoboCat;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FamilyServiceTest {
    private FamilyService familyService;

    @Before
    public void init() {
        FamilyDao familyDao = new CollectionFamilyDao();
        this.familyService = new FamilyService(familyDao);

        familyService.createNewFamily(new Human(), new Human());
    }

    @Test
    public void getAllFamilies() {
        assertEquals(1, familyService.getAllFamilies().size());
    }

    @Test
    public void getFamiliesBiggerThan() {
        assertTrue(familyService.getFamiliesBiggerThan(2).isEmpty());
    }

    @Test
    public void countFamiliesWithMemberNumber() {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);

        familyService.createNewFamily(new Human(), new Human());

        assertEquals( 0, familyService.countFamiliesWithMemberNumber(1).size());

    }

    @Test
    public void createNewFamily() {
        assertFalse(familyService.getAllFamilies().isEmpty());
    }

    @Test
    public void deleteFamilyByIndex() {
        assertTrue(familyService.deleteFamilyByIndex(0));
    }

    @Test
    public void bornChild() {
        familyService.bornChild(familyService.getFamilyById(0), "sasha", "masha");

        assertEquals(1, familyService.getFamilyById(0).getChildren().size());
    }

    @Test
    public void adoptChild() {
        Human kid = new Human();
        familyService.adoptChild(familyService.getFamilyById(0), kid);

        assertEquals(1, familyService.getFamilyById(0).getChildren().size());
    }

    @Test
    public void deleteAllChildrenOlderThen() {
        Human kid = new Human();
        familyService.adoptChild(familyService.getFamilyById(0), kid);
        familyService.deleteAllChildrenOlderThen(10);

        assertEquals(0, familyService.getFamilyById(0).getChildren().size());
    }

    @Test
    public void count() {
        assertEquals(1,  familyService.count());
    }

    @Test
    public void getFamilyById() {
        Human father = new Human();
        Human mother = new Human();
        Family expected = new Family(mother, father);
        familyService.createNewFamily(father, mother);
        assertEquals(expected, familyService.getFamilyById(0));
    }

    @Test
    public void getPets() {
        Pet pet = new RoboCat();
        familyService.addPet(0, pet);
        assertEquals(pet, familyService.getPets(0).get(0));
    }

    @Test
    public void addPet() {
        Pet pet = new RoboCat();
        familyService.addPet(0, pet);
        assertTrue(familyService.getPets(0).contains(pet));
    }
}