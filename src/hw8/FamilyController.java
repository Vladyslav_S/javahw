package hw8;

import hw8.humans.Human;
import hw8.pet.Pet;

import java.util.List;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int familySize) {
        return familyService.getFamiliesBiggerThan(familySize);
    }

    public List<Family> countFamiliesWithMemberNumber(int familySize) {
        return familyService.countFamiliesWithMemberNumber(familySize);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        return familyService.bornChild(family, boyName, girlName);
    }

    public Family adoptChild(Family family, Human kid) {
        return familyService.adoptChild(family, kid);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    void addPet(int familyIndex, Pet pet){
        familyService.addPet(familyIndex, pet);
    }
}
