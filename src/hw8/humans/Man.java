package hw8.humans;

public final class Man extends Human {
    @Override
    public void greetPet() {
       super.family.getPets().forEach(pet ->  System.out.println("Привет, " + pet.getNickname() + " Я мужик!"));
    }

    public void repairCar() {
        System.out.println("Бухаю по тихому");
    }
}
