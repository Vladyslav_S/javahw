package hw3;

import java.util.Locale;
import java.util.Scanner;

public class TaskPlanner {
    static Scanner scanner = new Scanner(System.in);
    static String[][] schedule = new String[7][2];
    static {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "work";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "sleep";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "eat";
        schedule[5][0] = "Friday";
        schedule[5][1] = "drink alcohol";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "workout";
    }

    public static void main(String[] args) {
        boolean flag = true;

        while (flag) {
            System.out.println("Please, input the day of the week:");

            String input = scanner.nextLine().trim();
            String[] inputSplit = input.split(" ");

            switch (inputSplit[0].toLowerCase(Locale.ROOT)) {
                case "sunday" -> System.out.printf("Your tasks for Sunday: %s.\n", schedule[0][1]);
                case "monday" -> System.out.printf("Your tasks for Monday: %s.\n", schedule[1][1]);
                case "tuesday" -> System.out.printf("Your tasks for Tuesday: %s.\n", schedule[2][1]);
                case "wednesday" -> System.out.printf("Your tasks for Wednesday: %s.\n", schedule[3][1]);
                case "thursday" -> System.out.printf("Your tasks for Thursday: %s.\n", schedule[4][1]);
                case "friday" -> System.out.printf("Your tasks for Friday: %s.\n", schedule[5][1]);
                case "saturday" -> System.out.printf("Your tasks for Saturday: %s.\n", schedule[6][1]);
                case "exit" -> flag = false;
                case "change" -> change(inputSplit);
                case "reschedule" -> change(inputSplit);
                default -> printErrorMsg();
            }
        }
    }

    private static void change(String[] inputSplit) {
        if (inputSplit.length < 2) {
            printErrorMsg();
            return;
        }

        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].toLowerCase(Locale.ROOT).equals(inputSplit[1].toLowerCase(Locale.ROOT))) {
                System.out.printf("Please, input new tasks for %s.\n", schedule[i][0]);
                schedule[i][1] = scanner.nextLine();
                return;
            }
        }
        printErrorMsg();
    }

    private static void printErrorMsg () {
        System.out.println("Sorry, I don't understand you, please try again.");
    }
}
