package hw4;

public class Main {
    public static void main(String[] args) {
        Human fatherVasia = new Human();
        Human motherVasia = new Human();
        Pet murzik = new Pet();
        String[][] schedule = {
                {"Monday", "eat"}
        };

        Human klara = new Human("Klara", "Pupkin", 1900);
        Human vasia = new Human("Vasia", "Pupkin", 1900, motherVasia, fatherVasia);

        Family family1 = new Family(klara, vasia);

        Human kid1 = new Human("Vasia", "Pupkin", 1974, 80,  klara, vasia, schedule);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);

        family1.setPet(murzik);
        family1.setChildren(new Human[]{kid1, kid2});
        family1.addChild(kid3);
        family1.deleteChild(1);

        murzik.eat();
        murzik.respond();
        murzik.foul();
        System.out.println(murzik);

        kid1.describePet();
        kid1.greetPet();
        System.out.println(kid1);
    }
}
