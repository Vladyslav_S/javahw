package hw5;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];

        mother.setFamily(this);
        father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        for (Human child : children) {
            child.setFamily(this);
        }
        this.children = children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    public void addChild(Human kid) {
        this.children = Arrays.copyOf(this.children, this.children.length + 1);
        kid.setFamily(this);
        this.children[this.children.length - 1] = kid;
    }

    public boolean deleteChild(int index) {
        if (this.children.length <= index || index < 0) {
            return false;
        }
        Human[] newChildren = new Human[this.children.length - 1];

        int counter = 0;
        for (int i = 0; i < this.children.length; i++) {
            if (i == index) {
                continue;
            }
            newChildren[counter] = this.children[i];
            counter++;
        }

        this.children = newChildren;
        return true;
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                Human[] newChildren = new Human[children.length - 1];
                System.arraycopy(children, 0, newChildren, 0, i);
                System.arraycopy(children, i + 1, newChildren, i, children.length - i - 1);
                children = newChildren;
                return true;
            }
        }
        return false;
    }

    public int countFamily() {
        int counter = 0;
        if (mother != null) {
            counter++;
        }
        if (father != null) {
            counter++;
        }
        return counter + children.length;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
