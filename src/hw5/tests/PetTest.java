package hw5.tests;

import hw5.Pet;
import hw5.enums.Species;
import org.junit.Test;

import static org.junit.Assert.*;

public class PetTest {

    @Test
    public void testToString() {
        Pet pet = new Pet(Species.CAT, "murzik");
        String result = pet.toString().split("[{]")[0];

        assertEquals(result, pet.getSpecies().toString());
    }
}