package hw5.tests;

import hw5.Human;
import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void testToString() {
        Human human = new Human();
        String result = human.toString().split("[{]")[0];

        assertEquals(result, "Human");
    }
}