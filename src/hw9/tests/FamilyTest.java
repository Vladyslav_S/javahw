package hw9.tests;

import hw9.Family;
import hw9.humans.Human;
import hw9.pet.Fish;
import hw9.pet.Pet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FamilyTest {

    @Test
    public void testToString() {
        Family family = new Family(new Human(), new Human());

        String result = family.toString().split("[{]")[0];

        assertEquals(result, "Family");
    }

    @Test
    public void deleteChildHumanArgument_shouldDeleteChosenChild() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(kid2);

        List<Human> expect = new ArrayList<>();
        expect.add(kid1);
        expect.add(kid3);
        assertEquals(family.getChildren(), expect);
    }

    @Test
    public void deleteChild_shouldNotDeleteChildIfItNotInTheFamily() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        Human randomKid = new Human();
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(randomKid);

        List<Human> expect = new ArrayList<>();
        expect.add(kid1);
        expect.add(kid2);
        expect.add(kid3);
        assertEquals(family.getChildren(), expect);
    }

    @Test
    public void deleteChildIndexArgument_shouldDeleteChosenChild() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(2);

        List<Human> expect = new ArrayList<>();
        expect.add(kid1);
        expect.add(kid2);
        assertEquals(family.getChildren(),expect);
    }


    @Test
    public void deleteChildByIndex_shouldNotDeleteChildIfItNotInTheFamily() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        Human kid3 = new Human("Serega", "Pupkin", 1976);
        Human randomKid = new Human();
        family.setChildren(new Human[]{kid1, kid2, kid3});

        family.deleteChild(4);

        List<Human> expect = new ArrayList<>();
        expect.add(kid1);
        expect.add(kid2);
        expect.add(kid3);
        assertEquals(family.getChildren(), expect);
    }

    @Test
    public void addChild() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Human kid2 = new Human("Masia", "Pupkin", 1975);
        family.setChildren(new Human[]{kid1});
        List<Human> childrenStartList = family.getChildren();

        family.addChild(kid2);

        assertEquals(family.getChildren().size(), childrenStartList.size());
        assertEquals(family.getChildren().get(1), kid2);
    }

    @Test
    public void countFamily() {
        Family family = new Family(new Human(), new Human());
        Human kid1 = new Human("Vasia", "Pupkin", 1974);
        Pet pet = new Fish();

        family.addChild(kid1);
        family.setPets(pet);

        assertEquals(family.countFamily(), 3);
    }
}