package hw9.pet;

import hw9.enums.Species;

import java.util.Set;

public class DomesticCat extends Pet implements Foulable {
    public DomesticCat(String nickname) {
        super(nickname);
        super.species = Species.DOMESTIC_CAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.DOMESTIC_CAT;
    }

    public DomesticCat() {
        super.species = Species.DOMESTIC_CAT;
    }

    @Override
    public void respond() {

    }

    @Override
    public void foul() {
        System.out.println("Kolbasa eliminated");
    }
}
