package hw9.pet;

import hw9.enums.Species;

import java.util.Set;

public class RoboCat extends Pet implements Foulable {

    public RoboCat(String nickname) {
        super(nickname);
        super.species = Species.ROBO_CAT;
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.species = Species.ROBO_CAT;
    }

    public RoboCat() {
        super.species = Species.ROBO_CAT;
    }

    @Override
    public void respond() {

    }

    @Override
    public void foul() {
        System.out.println("LAGALO!");
    }
}
