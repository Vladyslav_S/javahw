package hw9.humans;

import hw9.Family;
import hw9.enums.DayOfWeek;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Human mother;
    private Human father;
    private Map<DayOfWeek, String> schedule;
    protected Family family;

    public Human(String name, String surname, long milliSeconds) {
        this.name = name;
        this.surname = surname;
        this.birthDate = milliSeconds;
    }

    public Human(String name, String surname, long milliseconds, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.birthDate = milliseconds;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, long milliseconds, int iq, Human mother, Human father, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = milliseconds;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {
    }

    public String describeAge() {
        return new SimpleDateFormat("yyyy/MM/dd").format(new Date(birthDate));
    }

    public void greetPet() {
        System.out.print("Привет,");
        family.getPets().forEach(pet -> System.out.println(" " + pet));
    }

    public void describePet() {
        family.getPets()
                .forEach(pet -> {
                    String trickText = pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
                    System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + trickText);
                });
    }

    @Override
    public String toString() {
        return "Human{name='" + this.name + "', surname=" + this.surname + ", birthDate=" + new SimpleDateFormat("dd/MM/yyyy").format(new Date(birthDate)) + ", iq=" + this.iq + ", schedule=[" + (schedule) + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(mother, human.mother) && Objects.equals(father, human.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, mother, father);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }
}
