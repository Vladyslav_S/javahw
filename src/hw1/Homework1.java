package hw1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework1 {
    private static final String[][] FAMOUS_YEARS = {
            {"1998", "In what legendary year did Lazio take the legendary 7th place, gaining the legendary 56 points in the legendary Italian football championship"},
            {"2012", "In which year does Maia calendar end?"},
            {"1996", "In which year Java was released?"},
            {"1995", "In which year JavaScript was released?"},
    };
    private static int[] userNumbers = new int[1];

    public static void main(String[] args) {
        int randomNum = new Random().nextInt(FAMOUS_YEARS.length);
        int year = Integer.parseInt(FAMOUS_YEARS[randomNum][0]);

        System.out.println("Enter your name: ");

        Scanner sc = new Scanner(System.in);
        String userName = sc.nextLine();

        System.out.println("Let the game begin!");
        System.out.println(FAMOUS_YEARS[randomNum][1]);
        System.out.println("Enter your number:");

        while (true) {
            String userInput = sc.nextLine();
            int userNum;

            try {
                userNum = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                System.out.println("Not a number! Enter a number!");
                continue;
            }

            saveNumber(userNum);

            if (userNum == year) {
                System.out.printf("Congratulations, %s! \n", userName);
                Arrays.sort(userNumbers);
                System.out.println("Your numbers: " + Arrays.toString(userNumbers));
                break;
            } else if (userNum > year) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Your number is too small. Please, try again.");
            }
        }
    }

    private static void saveNumber(int number) {
        userNumbers = Arrays.copyOf(userNumbers, userNumbers.length + 1);
        userNumbers[userNumbers.length - 1] = number;
    }
}
